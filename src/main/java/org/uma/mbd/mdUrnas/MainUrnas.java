package org.uma.mbd.mdUrnas;

import org.uma.mbd.mdUrnas.urnas.Urna;

public class MainUrnas {

    public static void main(String[] args) {
        //Tomamos los datos del array de entrada
        try {
            int nb = Integer.parseInt(args[0]);
            int nn = Integer.parseInt(args[1]);


            Urna u = new Urna(nb, nn);

            int totalb = u.totalBolas();

            while (u.totalBolas() > 1) {
                Urna.ColorBola b1 = u.extraerBola();
                Urna.ColorBola b2 = u.extraerBola();
                if (b1 == b2) {
                    u.ponerBlanca();
                } else {
                    u.ponerNegra();
                }
            }
            System.out.println("La bola que queda es " + u.extraerBola());


        } catch (IllegalArgumentException e) {
            System.out.println("Error en Urna : " + e.getMessage());
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Error en Urna: Faltan argumentos ") ;

        }
        System.out.println("El programa finaliza ") ;


    }
}
