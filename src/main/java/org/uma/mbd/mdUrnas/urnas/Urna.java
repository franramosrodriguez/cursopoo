package org.uma.mbd.mdUrnas.urnas;

import java.util.NoSuchElementException;
import java.util.Random;

public class Urna {


    public static enum ColorBola {Blanca, Negra}
    private static Random alea = new Random();
    private int blancas;
    private int negras;

    public Urna(int nb, int nn) {
        if (nb < 0 || nn < 0 || nb + nn <= 0) {
            throw new IllegalArgumentException("Urna con argumentos erroneos");
        }
        blancas = nb;
        negras = nn;
    }

    public int totalBolas() {
        return blancas + negras;
    }

    public void ponerBlanca() {
        blancas++;
    }

    public void ponerNegra() {
        negras++;
    }

    public ColorBola extraerBola() {
        int total = totalBolas();
        if (total == 0) {
            throw new NoSuchElementException("No hay bolas");

        }
        ColorBola bolaSacada = null;

        int na = 1 + alea.nextInt(total);
        if (na <= blancas) {
            bolaSacada = ColorBola.Blanca;
            blancas--;
        } else {
            bolaSacada = ColorBola.Negra;
            negras--;
        }
        return bolaSacada;
    }

    @Override
    public String toString (){
        return "U(" + blancas + ", " + negras + ")";
    }
}

