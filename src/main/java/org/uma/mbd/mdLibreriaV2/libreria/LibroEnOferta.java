package org.uma.mbd.mdLibreriaV2.libreria;

import org.uma.mbd.mdLibreriaV2.libreria.Libro;

public class LibroEnOferta extends Libro {

    private double descuento;
    private double precioFinal;
    private double px = getPrecioBase() - getPrecioBase() * descuento / 100;

    public LibroEnOferta(String aut, String tit, double pb, double des) {

        super(aut, tit, pb);
        descuento = des;
    }

    public double getDescuento() {
        return descuento;
    }


    @Override
    public double getPrecioFinal() {

        double px = getPrecioBase() - getPrecioBase() * descuento / 100;
        precioFinal = px + px * getIVA() / 100;

        return precioFinal;
    }

    @Override
    public String toString() {
        double px = getPrecioBase() - getPrecioBase() * descuento / 100;
        return "(" + getAutor()
                + ";" + getTitulo()
                + ";" + getPrecioBase()
                + ";" + descuento
                + ";" + px
                + ";" + getIVA() + "%;"
                + ";" + precioFinal + ")";
    }
}
