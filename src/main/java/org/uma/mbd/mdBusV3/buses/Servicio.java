package org.uma.mbd.mdBusV3.buses;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;
import java.util.function.Predicate;

public class Servicio  {

    private String ciudad;
    private List <Bus> buses;

    public Servicio (String ciudad) {
        this.ciudad= ciudad;
        buses = new LinkedList<>();
    }

    public String getCiudad() {
        return ciudad;
    }

    public List <Bus> getBuses() {
        return buses;
    }


    public void leeBuses (String file) throws FileNotFoundException {
        try (Scanner sc = new Scanner( new File(file))) {
            leeBuses(sc);
        }
    }

    private void leeBuses (Scanner sc) {
        while (sc.hasNextLine()) {
            String linea = sc.nextLine();
            try {
                Bus bus = stringToBus(linea);
                buses.add(bus);
            } catch (InputMismatchException e) {
                System.err.println("Error dato no numérico en " + linea);
            } catch (NoSuchElementException e) {
                System.err.println("Error faltan datos en " + linea);
            }
        }
    }

    private Bus stringToBus (String linea) {
        try (Scanner sc = new Scanner(linea)) {
            sc.useDelimiter("[,+]");
            int cb = sc.nextInt();
            String ma = sc.next();
            int cl = sc.nextInt();
            Bus bus = new Bus(cb, ma);
            bus.setCodLinea(cl);
            return bus;
        }
    }

    public List <Bus> filtra (Comparator <Bus> comp, Predicate<Bus> ctr) {
        List <Bus> sol = new ArrayList<>();
        int pos = 0;
        for (Bus bus: buses) {
            if (ctr.test(bus)) {
                sol.add(bus);
            }
        }

        sol.sort(comp);

        return new ArrayList<>(sol);
    }

    public void guarda (String file, Comparator <Bus> comp ,Predicate<Bus>  ctr) throws FileNotFoundException {
        try (PrintWriter pw = new PrintWriter(file)) {
            guarda (pw,comp, ctr);
        }
    }

    public void guarda (PrintWriter pw, Comparator <Bus> comp ,Predicate<Bus>  ctr) {
        List <Bus> cumplen = filtra(comp, ctr);
        pw.println(ctr);
        for (Bus bus : cumplen) {
            pw.println(bus);
        }
    }



}
