package org.uma.mbd.mdBusV3.buses;

public class Bus implements Comparable <Bus> {
    private String matricula;
    private int codBus;
    private int codLinea;

    public Bus (int bus, String ma) {
        matricula= ma;
        codBus= bus;
    }

    public String getMatricula() {
        return matricula;
    }

    public int getCodBus() {
        return codBus;
    }

    public int getCodLinea() {
        return codLinea;
    }

    public void setCodLinea(int codLinea) {
        this.codLinea = codLinea;
    }

    @Override
    public boolean equals (Object o) {
        boolean res = o instanceof Bus;
        Bus b = res ? (Bus)o : null;
        return res && (b.matricula.equalsIgnoreCase(matricula)) && (codBus==b.codBus);
    }

    @Override
    public int hashCode () {
        return matricula.toUpperCase().hashCode() + Integer.hashCode(codBus);
    }

    @Override
    public int compareTo (Bus bus) {
        int res = matricula.compareTo(bus.matricula);
        return res;
    }


    @Override
    public String toString() {
        return   "Bus(" + codBus + ","+ matricula + ","+ codLinea+")";
    }
}
