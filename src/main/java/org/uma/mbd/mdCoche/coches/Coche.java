package org.uma.mbd.mdCoche.coches;

public class Coche {

    private String nombre;
    private double precio;
    private static double PIVA = 0.16;

    public Coche(String nombre, double precio) {
        this.nombre = nombre;
        this.precio = precio;
    }

    public String getNombre() {
        return nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public static double getPIVA() {
        return PIVA;
    }

    public static void setPiva(double PIVA) {
        Coche.PIVA = PIVA;
    }


    public double precioTotal () {
        double pt = precio*PIVA;
        return  pt;
    }

    @Override
    public String toString () {
        double pt = precio*PIVA;
        return nombre + " -> " + pt;
    }




}
