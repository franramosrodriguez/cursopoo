package org.uma.mbd.mdTest.test;
import org.uma.mbd.mdTest.test.Test;
import java.io.File;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;


public class TestAsignatura {

    private String nombre;
    private double valorAciertos;
    private double valorErrores;
    private Test [] examenes;
    private static final double APROBADO = 5;



    public TestAsignatura(String n, String[] datos) {
        this (n,1,0,datos);
    }

    public TestAsignatura(String n, double va, double vf ,String[] datos) {

        nombre = n;
        valorAciertos= va;
        valorErrores= vf;
        extraeDatos(datos);
    }

    public TestAsignatura (String n, double va, double vf) {
        nombre = n;
        valorAciertos= va;
        valorErrores= vf;
    }

    public void leeDatos (String file) throws FileNotFoundException {
        try (Scanner sc = new Scanner( new File (file))) {
            leeDatos(sc);
        }
    }

    public void leeDatos (Scanner sc) {
        examenes = new Test[5];
        int pos = 0;
        while (sc.hasNextLine()) {
            String linea = sc.nextLine();
            Test test = stringToTest(linea);
            if (pos == examenes.length) {
                examenes = Arrays.copyOf(examenes, examenes.length*2);
            }
            examenes[pos]= test;
            pos++;
        }
        examenes = Arrays.copyOf(examenes, pos);
    }

    private Test stringToTest (String linea) {
        try (Scanner sc = new Scanner(linea)) {
            sc.useDelimiter("[:+]");
            String nombreAl = sc.next();
            int ac = sc.nextInt();
            int er = sc.nextInt();
            Test test = new Test (nombreAl, ac, er);
            return test;
        }
    }


    public void extraeDatos (String[] datos) {

        examenes = new Test[datos.length];
        for (int i =0; i< datos.length; i++) {
            Test test = stringToTest(datos[i]);
            examenes[i]= test;
        }
    }

    public Test[] getExamenes() {
        return examenes;
    }

    public double notaMedia() {
        double sum =0;
        for (Test test : examenes) {
            sum+= test.getCalificacion(valorAciertos,valorErrores);
        }
        return sum/ examenes.length;
    }

    public int aprobados() {
        int na = 0;
        for (Test test : examenes) {
            if (test.getCalificacion(valorAciertos, valorErrores) >= APROBADO) {
                na++;
            }
        }
        return na;
    }


    public String getNombre() {
        return nombre;
    }

    public Test[] testAprobados () {
        Test [] testAp = new Test[examenes.length];
        int pos;
        pos = 0;
        for (Test test : examenes) {
            if (test.getCalificacion(valorAciertos, valorErrores) >= APROBADO){
                testAp[pos]= test;
                pos++;
            }
        }
        return Arrays.copyOf(testAp, pos);
    }


}
