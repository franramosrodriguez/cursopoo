package org.uma.mbd.mdTest.test;
import java.util.StringJoiner;

public class Test {

    private String alumno;
    private int aciertos;
    private int errores;

    public Test (String alumno, int aciertos, int errores) {
        this.alumno = alumno;
        this.errores = errores;
        this.aciertos= aciertos;
    }


    public String getAlumno () {
        return alumno;
    }

    public int getAciertos () {
        return aciertos;
    }

    public int getErrores () {
        return errores;
    }

    @Override
    public boolean equals (Object o) {
        boolean res = o instanceof Test;
        Test t = res ? (Test)o : null;
        return res && (t.alumno.equalsIgnoreCase(alumno));
    }

    @Override
    public int hashCode () {
        return alumno.toLowerCase().hashCode();
    }


    @Override
    public String toString() {
        return  alumno.toUpperCase() + " [" + aciertos + ","+ errores + "]";
    }

    public double getCalificacion (double valAc, double valErr) {

        if (valAc <=0) {
            throw new RuntimeException ("Error. Valoracion de aciertos negativa");
        }

        if (valErr >0) {
            throw new RuntimeException ("Error. Valoracion de errores positiva");
        }

        double calificacion = aciertos*valAc + valErr*errores;
          return calificacion;
    }



}
