package org.uma.mbd.mdTest;

import org.uma.mbd.mdTest.test.Test;



import org.uma.mbd.mdTest.test.TestAsignatura;

import java.io.FileNotFoundException;
import java.util.Arrays;

public class PruebaTest {
        static String [] examenes ={
                "El hombre enmascarado:6+4",
                "La mujer Maravilla:9+1",
                "El hombre invisible:4+6",
                "Dr. Jekyll y Mr. Hide:5+5",
                "El increible hombre menguante:7+3",
                "El llanero solitario:7+3",
                "La chica invisible:8+2",
                "La princesa guerrera:2+8" };

        public static void main(String[] args) throws  FileNotFoundException {
  //          TestAsignatura poo = new TestAsignatura(
  //                  "Programación Orientada a Objetos", 1, 0, examenes);
            TestAsignatura poo = new TestAsignatura (
                    "Programación Orientada a Objetos", 1, -0.33);

            poo.leeDatos("recursos/mdTest/test.txt");


            System.out.println("Asignatura: " + poo.getNombre());
            System.out.println("Aprobados en el test: " + poo.aprobados());
            System.out.println("Nota media en el test: " + poo.notaMedia());
            System.out.println("Aprobados " + Arrays.toString(poo.testAprobados()));
        }
    }




