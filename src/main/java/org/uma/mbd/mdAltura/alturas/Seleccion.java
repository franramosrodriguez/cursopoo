package org.uma.mbd.mdAltura.alturas;

public interface Seleccion {
    boolean test (Pais pais);
}
