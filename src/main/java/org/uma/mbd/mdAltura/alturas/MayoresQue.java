package org.uma.mbd.mdAltura.alturas;

public class MayoresQue implements Seleccion {

    private double altura;

    public MayoresQue(double altura) {
        this.altura = altura;
    }


    public boolean test (Pais pais) {
        return pais.getAltura() > altura;
    }




}
