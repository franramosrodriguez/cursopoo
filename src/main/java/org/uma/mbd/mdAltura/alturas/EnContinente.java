package org.uma.mbd.mdAltura.alturas;

public class EnContinente implements Seleccion  {

    private String texto;

    public EnContinente(String texto) {
        this.texto = texto;
    }

    @Override
    public boolean test (Pais pais) {
        return pais.getContinente() == texto;
    }



}
