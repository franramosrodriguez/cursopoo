package org.uma.mbd.mdAltura.alturas;

import org.uma.mbd.mdBusV2.buses.Bus;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.function.Predicate;

public class Paises {
    private List<Pais> paises;

    public Paises() {
        paises =  new LinkedList<>();
    }

    public List<Pais> getPaises() {
        return paises;
    }

    public void leePaises (String file) throws FileNotFoundException {
        try (Scanner sc = new Scanner( new File(file))) {
            leePaises(sc);
        }
    }

    private void leePaises (Scanner sc) {
        while (sc.hasNextLine()) {
            String linea = sc.nextLine();
            try {
                Pais pais = stringToPais(linea);
                paises.add(pais);


            } catch (InputMismatchException e) {
                System.err.println("Error dato no numérico en " + linea);
            } catch (NoSuchElementException e) {
                System.err.println("Error faltan datos en " + linea);
            }
        }
    }

    private Pais stringToPais (String linea) {
        try (Scanner sc = new Scanner(linea)) {
            sc.useLocale(Locale.ENGLISH);
            sc.useDelimiter("[,]");
            String np = sc.next();
            String nc = sc.next();
            double al = sc.nextDouble();
            Pais pais = new Pais(np,nc,al);
            return pais;
        }
    }

    public List <Pais> selecciona (Predicate <Pais> sel) {
        List <Pais> sol = new ArrayList<>();
        int pos = 0;
        for (Pais pais: paises) {
            if (sel.test(pais)) {
                sol.add(pais);
            }
        }
        return sol;
    }

    public Set <String> getContinentes () {
        Set <String> set = new TreeSet<>(Comparator.<String>naturalOrder().reversed());
        for (Pais pais: paises) {
            set.add(pais.getContinente());
        }
        return set;
    }

    public Set <Double> getAltura () {
        Set <Double> set = new TreeSet<>();
        for (Pais pais: paises) {
            set.add(pais.getAltura());
        }
        return set;
    }


}
