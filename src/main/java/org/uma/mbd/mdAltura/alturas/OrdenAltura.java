package org.uma.mbd.mdAltura.alturas;

import java.util.Comparator;

public class OrdenAltura implements Comparator<Pais> {
    //Se comparan por altura

    @Override
    public int compare (Pais p1, Pais p2) {
        return Double.compare(p1.getAltura(), p2.getAltura());
    }



}
