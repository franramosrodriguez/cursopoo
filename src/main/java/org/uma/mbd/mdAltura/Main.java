package org.uma.mbd.mdAltura;

import org.uma.mbd.mdAltura.alturas.*;

import java.io.FileNotFoundException;
import java.util.*;
import java.util.function.Predicate;

public class Main {
    public static void main(String args[]) throws FileNotFoundException {
        Paises paises = new Paises();
        paises.leePaises("recursos/mdAlturas/alturas.txt");

        System.out.println("Menores que 1.7");
        Predicate <Pais> pred = p -> p.getAltura() < 1.7;
        List <Pais> lista = paises.selecciona(pred);



        Comparator <Pais> oNombre = new OrdenNombre();
        Comparator <Pais> oContinente = new OrdenContienente();
        Comparator <Pais> oAltura = new OrdenAltura();

        Comparator <Pais> oNombre1 = (p1,p2) -> p1.getNombre().compareTo(p2.getNombre());
        Comparator <Pais> oContinente1 = (p1,p2) -> p1.getContinente().compareTo(p2.getContinente());
        Comparator <Pais> oAltura1 = (p1,p2) -> Double.compare(p1.getAltura(),p2.getAltura());


        // Otra forma (Programacion funcional
        Comparator <Pais> oNombre2 = Comparator.comparing(Pais::getNombre);


        Comparator <Pais> comp = oContinente
                                    .thenComparing(oAltura.reversed())
                                    .thenComparing(oNombre.reversed());
        //lista.sort(comp);

        // Orden natural
        //Set <Pais> sp =new TreeSet<>(lista);

        //Orden continente;
        Set <Pais> spa = new TreeSet<>(oContinente);
        spa.addAll(lista);



        for (Pais pais : lista) {
            System.out.println(pais);
        }


        System.out.println("En Europa");
        Predicate <Pais> pred2 = p -> p.getContinente().equals("Europe");
        List <Pais> lista2 = paises.selecciona(pred2);
        Comparator <Pais> comp2 = oAltura;
        Collections.sort(lista2, comp2);
        for (Pais pais : lista2) {
            System.out.println(pais);
        }

        System.out.println("Conjuntos");
        System.out.println(paises.getContinentes());
        System.out.println(paises.getAltura());





    }
}
