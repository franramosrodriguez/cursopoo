package org.uma.mbd.mdJarras.jarras;


public class Mesa {

    private Jarra ja;
    private Jarra jb;


    public Mesa(Jarra ja, Jarra jb){
        this.ja = ja;
        this.jb = jb;
    }

    public void llenaA () {
        ja.llena();
    }

    public void llenaB () {
        jb.llena();
    }

    public void vaciaA () {ja.vacia();}

    public void vaciaB () {jb.vacia();}

    public void vuelcaAsobreB(){
        jb.llenaDesde(ja);
    }

    public void vuelcaBsobreA() {
        ja.llenaDesde(jb);
    }

    public int getCapacidadA(){
        return ja.getCapacidad();
    }

    public int getCapacidadB(){
        return jb.getCapacidad();
    }

    public int getContenidoA(){
        return ja.getContenido();
    }

    public int getContenidoB(){
        return jb.getContenido();
    }

    public int getContenido (){
        int a = ja.getContenido()+jb.getContenido();
        return a;
    }

    @Override
    public String toString(){
        return "Jarra A: (" + ja.getContenido() + ", "+ ja.getCapacidad() + ") \nJarra B: (" +
                jb.getContenido() + ", "+ jb.getCapacidad() + ")";

    }

}
