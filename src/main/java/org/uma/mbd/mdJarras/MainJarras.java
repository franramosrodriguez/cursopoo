package org.uma.mbd.mdJarras;
import org.uma.mbd.mdJarras.jarras.Jarra;

public class MainJarras {
    public static void main (String [] args) {
        try {
            Jarra ja = new Jarra(25);
            Jarra jb = new Jarra(7);
            jb.llena();
            ja.llenaDesde(jb);
            ja.vacia();
            ja.llenaDesde(jb);
            jb.llena();
            ja.llenaDesde(jb);
            System.out.println("ja = " + ja.getContenido());
            System.out.println("jb = " + jb.getContenido());
        } catch (IllegalArgumentException e)
        {
            System.out.println("Error ");
        }
    }

}
