package org.uma.mbd.mdLibreriaV1.libreria;

import java.util.Arrays;

public class LibreriaV11111 {

    private static int TAM_DEFECTO = 16;
    private int numLibros =0;
    private org.uma.mbd.mdLibreriaV1.libro.LibroV1111[] libros;



    public LibreriaV11111() {
    libros = new org.uma.mbd.mdLibreriaV1.libro.LibroV1111[TAM_DEFECTO];
    }

    public LibreriaV11111(int d) {
        libros = new org.uma.mbd.mdLibreriaV1.libro.LibroV1111[d];
    }

    public void addLibro (String aut, String tit, double pb) {

        if (numLibros >= libros.length) {
            libros = Arrays.copyOf(libros, libros.length*2);
        } else {
            libros[numLibros] = new org.uma.mbd.mdLibreriaV1.libro.LibroV1111(aut, tit, pb);
            numLibros++ ;

        }
    }

    public void remLibro (String aut, String tit) {
    }

    public void getPrecioBase (String aut, String tit) {

    }

    public void getPrecioFinal (String aut, String tit) {

    }

}
