package org.uma.mbd.mdLibreriaV1.libro;

public class LibroV1111 {

    private String autor;
    private String titulo;
    private double precioBase;
    private double IVA = 0.1;

    public LibroV1111(String autor, String titulo, double precioBase){

        this.autor= autor;
        this.titulo= titulo;
        this.precioBase=precioBase;

    }

    public String getAutor(){
        return autor;
    }

    public String getTitulo(){
        return titulo;
    }

    public double getPrecioBase(){
        return precioBase;
    }

    public double getPreioFinal(){
        return (precioBase+precioBase*IVA);
    }

    public double getIVA(){
        return IVA;
    }

    public void setIVA (double d) {
        IVA = d;
    }

    @Override
    public String toString(){
        return "(" + autor + "; "+ titulo + "; "+ precioBase+ "; "+ IVA + "; "+ this.getPreioFinal() +")";
    }



}
