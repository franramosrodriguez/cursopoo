package org.uma.mbd.mdPruebaStream;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {
        Random rnd = new Random();

        IntStream is = rnd.ints(1000, 0,6);
        // Simulamos 1000 números entre 0 y 6

        is
                .map(n -> n+1)  //A cada uno se suma 1
                .filter(n -> n>4)
                .limit(20)
                .forEach(n -> System.out.println(n));

        IntStream oc = IntStream.rangeClosed(1000, 1600);
        List <Integer> list = Arrays.asList(3,4,5,6,7,8,6,4,9,4,5,6,7,1);
        list.stream()
                    .filter(n -> n%3 ==0)
                    .forEach(System.out::println);




        IntStream ad = rnd.ints(1000, 0,6);



    }
}
