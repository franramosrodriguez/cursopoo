package org.uma.mbd.mdMusculacion.musculacion;

public class Maquina implements Comparable <Maquina>{
    private String descripcion;
    private String nombre;
    private int nivel;
    private String funcion;
    private int maquinaId;
    private String urlImagen;

    public Maquina(int maquinaId, String nombre ) {
        this.nombre = nombre;
        this.maquinaId = maquinaId;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public int getNivel() {
        return nivel;
    }

    public String getFuncion() {
        return funcion;
    }

    public int getMaquinaId() {
        return maquinaId;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public void setFuncion(String funcion) {
        this.funcion = funcion;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    @Override
    public int compareTo (Maquina m) {
        int res = Integer.compare(maquinaId, m.maquinaId);

        return res;
    }

    @Override
    public String toString () {
        return "Maquina(" + maquinaId + " , "+ nombre + ")";
    }



}
