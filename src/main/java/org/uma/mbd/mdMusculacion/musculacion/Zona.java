package org.uma.mbd.mdMusculacion.musculacion;

import java.util.Set;
import java.util.TreeSet;

public class Zona implements Comparable <Zona> {

    private int zonaId;
    private String nombre;
    private double latitud;
    private double longitud;
    private String urlImagen;
    private Set <Maquina> maquinas;

    public Zona(int zonaId, String nombre) {
        this.zonaId = zonaId;
        this.nombre = nombre;
        maquinas= new TreeSet<>();
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public int getZonaId() {
        return zonaId;
    }

    public String getNombre() {
        return nombre;
    }

    public double getLatitud() {
        return latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public Set<Maquina> getMaquinas() {
        return maquinas;
    }

    public void agrega (Maquina mq) {
        maquinas.add(mq);
    }

    @Override
    public int compareTo (Zona z) {
        int res = Integer.compare(zonaId, z.zonaId);

        return res;
    }

    @Override
    public String toString () {
        return "Zona("+ zonaId+ ", "+nombre+")";
    }



}
