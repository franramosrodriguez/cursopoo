package org.uma.mbd.mdBusV1.buses;

public class PorLinea implements Criterio{

    private int codLinea;

    public PorLinea(int codLinea) {
        this.codLinea = codLinea;
    }

    @Override
    public boolean esSeleccionable (Bus bus) {
        boolean res = false;
        if (bus.getCodLinea()== codLinea) {
            res = true;
        }
        return res;
    }

    @Override
    public String toString () {
        return "Autobuses de la linea" + codLinea;
    }



}
