package org.uma.mbd.mdBusV1.buses;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Servicio {

    private String ciudad;
    private Bus [] buses;
    private static int TAM = 10;

    public Servicio (String ciudad) {
        this.ciudad= ciudad;
        buses = new Bus[TAM];
    }

    public String getCiudad() {
        return ciudad;
    }

    public Bus[] getBuses() {
        return buses;
    }


    public void leeBuses (String file) throws FileNotFoundException {
        try (Scanner sc = new Scanner( new File(file))) {
            leeBuses(sc);
        }
    }

    private void leeBuses (Scanner sc) {
        int pos = 0;
        while (sc.hasNextLine()) {
            String linea = sc.nextLine();
            try {

                Bus bus = stringToBus(linea);
                if (pos == buses.length) {
                    buses = Arrays.copyOf(buses, buses.length * 2);
                }
                buses[pos] = bus;
                pos++;
            } catch (InputMismatchException e) {
                System.err.println("Error dato no numérico en " + linea);
            } catch (NoSuchElementException e) {
                System.err.println("Error faltan datos en " + linea);
            }
        }
        buses = Arrays.copyOf(buses, pos);

    }

    private Bus stringToBus (String linea) {
        try (Scanner sc = new Scanner(linea)) {
            sc.useDelimiter("[,+]");
            int cb = sc.nextInt();
            String ma = sc.next();
            int cl = sc.nextInt();
            Bus bus = new Bus (cb, ma);
            bus.setCodLinea(cl);
            return bus;
        }
    }

    public Bus [] filtra (Criterio ctr) {
        Bus [] sol = new Bus [buses.length];
        int pos = 0;
        for (Bus bus: buses) {
            if (ctr.esSeleccionable(bus)) {
                sol[pos] = bus;
                pos++;
            }
        }
        return Arrays.copyOf(sol, pos);
    }

    public void guarda (String file, Criterio ctr) throws FileNotFoundException {
        try (PrintWriter pw = new PrintWriter(file)) {
            guarda (pw, ctr);
        }
    }

    public void guarda (PrintWriter pw, Criterio ctr) {
        Bus [] cumplen = filtra(ctr);
        pw.println(ctr);
        for (Bus bus : cumplen) {
            pw.println(bus);
        }
    }



}
