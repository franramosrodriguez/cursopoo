package org.uma.mbd.mdBusV1;

import org.uma.mbd.mdBusV1.buses.Bus;
import org.uma.mbd.mdBusV1.buses.Servicio;

import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Servicio servicio = new Servicio( "Malaga");
        servicio.leeBuses("recursos/mdBus/buses.txt");
        for (Bus bus : servicio.getBuses()) {
            System.out.println(bus);
        }
    }
}
