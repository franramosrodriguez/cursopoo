package org.uma.mbd.mdKWIC.kwic;

public class TituloKWIC implements Comparable <TituloKWIC> {
    private String titulo;
    private int posicion;

    public TituloKWIC(String titulo, int posicion) {
        this.titulo = titulo;
        this.posicion = posicion;
    }

    @Override
    public boolean equals (Object o) {
        boolean res = o instanceof TituloKWIC;
        TituloKWIC t = res ? (TituloKWIC)o : null;
        return res && (t.titulo.equalsIgnoreCase(titulo)) && (posicion==t.posicion);
    }

    @Override
    public int hashCode () {
        return titulo.toUpperCase().hashCode() + Integer.hashCode(posicion);
    }

    @Override
    public int compareTo (TituloKWIC t){
        int res = titulo.compareToIgnoreCase(t.titulo);
        if (res == 0) {
            res = Integer.compare(posicion, t.posicion);
        }
        return res;
    }

    @Override
    public String toString () {
        return titulo + " ("+ posicion+ ")";
    }
}
