package org.uma.mbd.mdKWIC.kwic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

public class KWIC {

    private Set<String> palNoSig;
    private Map <String, Set<TituloKWIC>> indice;

    public KWIC () {
        palNoSig = new TreeSet<>();
        indice = new TreeMap<>();
    }


    public void palabrasNoSignificativas (String file) throws FileNotFoundException {
        try (Scanner sc = new Scanner( new File(file))) {
            palabrasNoSignificativas(sc);
        }
    }

    public void palabrasNoSignificativas (Scanner sc) {
        while (sc.hasNextLine()) {
            String linea = sc.nextLine();
            try ( Scanner scLinea = new Scanner(linea)) {
                scLinea.useDelimiter(" ");
                while (scLinea.hasNextLine()) {
                    String palabra = scLinea.next().toUpperCase();
                    palNoSig.add(palabra);
                }
            } catch (InputMismatchException e) {
                System.err.println("Error dato no numérico en " + linea);
            } catch (NoSuchElementException e) {
                System.err.println("Error faltan datos en " + linea);
            }
        }
    }

    public void generaIndice (String file) throws FileNotFoundException {
        try (Scanner sc = new Scanner( new File(file))) {
            generaIndice(sc);
        }
    }

    public void generaIndice (Scanner sc) {
        while (sc.hasNextLine()) {
            String titulo = sc.nextLine();
            int pos = 0;
            try (Scanner scLinea = new Scanner(titulo)) {
                scLinea.useDelimiter("[ :]+");
                while (scLinea.hasNext()) {
                    String palabra = scLinea.next().toUpperCase();
                    if (!palNoSig.contains(palabra)) {
                        TituloKWIC tk = new TituloKWIC(titulo, pos);
                        Set<TituloKWIC> set = indice.computeIfAbsent(palabra, k -> new TreeSet<>());
                        set.add(tk);
                    }
                    pos++;
                }
            }
        }
    }

    public void presentaIndice (String file) throws FileNotFoundException {
        try (PrintWriter pw = new PrintWriter(file)) {
            presentaIndice(pw);
        }
    }

    public void presentaIndice (PrintWriter pw) {
        for (String palabra: indice.keySet()) {
            pw.println(palabra);
            for (TituloKWIC tk: indice.get(palabra)) {
                pw.println("\t" + tk);
            }
        }
    }

}
