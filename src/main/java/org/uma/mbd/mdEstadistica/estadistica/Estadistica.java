package org.uma.mbd.mdEstadistica.estadistica;

public class Estadistica {
    public double numElementos =0;
    public double sumaX =0;
    public double sumaX2=0;

    /*
    public Estadistica(double d){
        numElementos=1;
        sumaX=d;
        sumaX2=(d*d);

    }
    */

    public void agrega (double d){
        numElementos ++;
        sumaX += +d;
        sumaX2 += + Math.pow(d,2);
    }


    public void agrega (double d, int n){
        numElementos = numElementos+n;
        sumaX=sumaX +d*n;
        sumaX2=sumaX2 + Math.pow(d,2)*n;
    }

    public double media (){
        return sumaX/numElementos;
    }

    public double varianza (){
        return ((sumaX2 / numElementos) - ((sumaX/numElementos)*(sumaX/numElementos)));
    }


}
