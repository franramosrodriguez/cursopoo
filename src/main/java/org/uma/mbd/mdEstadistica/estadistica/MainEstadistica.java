package org.uma.mbd.mdEstadistica.estadistica;
import java.util.Random;
import org.uma.mbd.mdEstadistica.estadistica.Estadistica;
/**
 * Ejemplo de uso de la clase estadistica
 * @author POO
 *
 */
public class MainEstadistica {
    public static void main(String [] args) {

        Estadistica est = new Estadistica();
        Random ran = new Random();
        for (int i = 0; i < 100000 ; i++) {
            est.agrega(ran.nextGaussian());
        }
        System.out.println("Media = "+est.media());
        System.out.println("Número de elementos = "+est.numElementos);
        System.out.println("Varianza = "+est.varianza());
        System.out.println("sumaX = "+est.sumaX);

    }
}
