package org.uma.mbd.mdLibreriaV3.libreria;

public class OfertaPrecio implements OfertaFlex {

    private double porDescuento;
    private double umbralPrecio;

    public OfertaPrecio (double desc, double umbral) {

        porDescuento = desc;
        umbralPrecio= umbral;
    }

    @Override
    public double getDescuento (Libro libro) {
        return libro.getPrecioBase() >= umbralPrecio ? porDescuento : 0;
    }

    @Override
    public String toString () {
        return porDescuento + "%()" + umbralPrecio + ")";
    }

}
