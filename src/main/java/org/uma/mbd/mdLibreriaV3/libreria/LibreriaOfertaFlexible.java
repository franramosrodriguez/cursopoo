package org.uma.mbd.mdLibreriaV3.libreria;

public class LibreriaOfertaFlexible extends Libreria {

    //Estado
    private OfertaFlex ofertaFlexible;

    // Constructores
    public LibreriaOfertaFlexible( OfertaFlex oferta) {

        this (TAM_DEFECTO, oferta);
    }

    public LibreriaOfertaFlexible(int tam, OfertaFlex oferta) {
        super (tam);
        ofertaFlexible = oferta;
    }

    @Override
    public void addLibro (String autor, String titulo, double precioBase) {
        Libro libro = new Libro (autor, titulo, precioBase);
        double descuento = ofertaFlexible.getDescuento(libro);
        if (descuento> 0) {
            libro = new LibroEnOferta(autor, titulo, precioBase, descuento);
        }
        addLibro(libro);
    }

    @Override
    public String toString () {
        return super.toString();

    }

}
