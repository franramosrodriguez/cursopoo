package org.uma.mbd.mdPruebaMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CuentaPosiciones {
    public static void main(String[] args) {
        Map< String, List<Integer>> map = new HashMap<>();

        for (int i = 0; i < args.length; i++) {
            String palabra = args [i];
            List<Integer> lista = map.computeIfAbsent(palabra, k -> new ArrayList<>());
            lista.add(i);
            /*
            List <Integer> lista = map.get(palabra);
            if (lista == null) {
                lista = new ArrayList<>();
                map.put(palabra, lista);
            }
            lista.add(i);
            */
            /*
            if (map.containsKey(palabra)) {
                List <Integer> lista = map.get(palabra);
                lista.add(i);
            } else {
                List <Integer> lista = new ArrayList<>();
                lista.add(i);
                map.put(palabra, lista);
            }
            */
        }
        for (Map.Entry <String, List<Integer>> entrada : map.entrySet()) {
            System.out.println(entrada.getKey()
            + " aparece en las posiciones " + entrada.getValue());
        }
    }
}
