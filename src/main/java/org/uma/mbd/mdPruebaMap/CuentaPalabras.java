package org.uma.mbd.mdPruebaMap;

import java.util.HashMap;
import java.util.Map;

public class CuentaPalabras {
    public static void main(String[] args) {

        Map<String, Integer> map = new HashMap<>();
        for (String palabra : args) {
            /*
            Integer i = map.get(palabra);
            if (i == null) {
                map.put (palabra, 1);
            } else {
                map.put (palabra, i+1);
            }
                */

            int i = map.getOrDefault(palabra, 0);
            map.put(palabra, i+1);

        }

        System.out.println("\nPrimera forma \n");

        for (String s : map.keySet()){
            System.out.println(s + " aparece "+ map.get(s));
        }
        System.out.println("\n \nSegunda forma \n");


        for (Map.Entry<String, Integer> entrada : map.entrySet()){
            System.out.println(entrada.getKey() + " aparece "+ entrada.getValue());
        }


    }
}
