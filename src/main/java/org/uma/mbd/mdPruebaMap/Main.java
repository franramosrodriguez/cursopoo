package org.uma.mbd.mdPruebaMap;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        Map<String, Integer> map = new HashMap<>();
        map.put("Hola", 4);
        map.put("Que", 3);
        map.put("Tal", 3);
        map.put("Adios",5);

        System.out.println(map);
        System.out.println(map.containsKey("Que"));
        System.out.println(map.containsValue(3));
        Set<String> set= map.keySet();
        System.out.println(set);
        Collection <Integer> col = map.values();
        System.out.println(col);

        for (String s: map.keySet()){
            System.out.println(s + " "+ map.get(s));
        }

        System.out.println(map.get("Estamos"));






    }
}
