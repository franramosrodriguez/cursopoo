package org.uma.mbd.mdAlturaV2.alturas;

import java.util.Objects;

public class Pais implements Comparable <Pais>  {
    private String nombre;
    private String continente;
    private double altura;

    public Pais(String nombre, String continente, double altura) {
        this.nombre = nombre;
        this.continente = continente;
        this.altura = altura;
    }

    public String getNombre() {
        return nombre;
    }

    public String getContinente() {
        return continente;
    }

    public double getAltura() {
        return altura;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pais)) return false;
        Pais pais = (Pais) o;
        return Objects.equals(getNombre(), pais.getNombre());
    }

    @Override
    public int compareTo (Pais pais) {
        int res = Double.compare(altura, pais.altura);
        if (res ==0) {
            res = nombre.compareTo(pais.nombre);
        }
        return res;
    }

    @Override
    public int hashCode() {
        return nombre.hashCode();
    }

    @Override
    public String toString() {
        return "Pais("+ nombre +", " + continente + ", " + altura + ")";
    }




}
