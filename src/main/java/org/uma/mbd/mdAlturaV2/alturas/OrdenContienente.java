package org.uma.mbd.mdAlturaV2.alturas;

import java.util.Comparator;

public class OrdenContienente implements Comparator<Pais> {
    //Se comparan por altura

    @Override
    public int compare (Pais p1, Pais p2) {
        return p1.getContinente().compareTo(p2.getContinente());
    }


}
