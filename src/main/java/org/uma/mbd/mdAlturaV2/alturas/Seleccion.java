package org.uma.mbd.mdAlturaV2.alturas;

public interface Seleccion {
    boolean test(Pais pais);
}
