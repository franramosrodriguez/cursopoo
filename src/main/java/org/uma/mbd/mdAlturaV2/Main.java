package org.uma.mbd.mdAlturaV2;

import org.uma.mbd.mdAlturaV2.alturas.*;

import java.io.FileNotFoundException;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Main {
    public static void main(String args[]) throws FileNotFoundException {
        Paises paises = new Paises();
        paises.leePaises("recursos/mdAlturas/alturas.txt");

        System.out.println("Menores que 1.7");
        Predicate<Pais> pred = p -> p.getAltura() < 1.7;
        List<Pais> lista = paises.selecciona(pred);



        Comparator<Pais> oNombre = new OrdenNombre();
        Comparator <Pais> oContinente = new OrdenContienente();
        Comparator <Pais> oAltura = new OrdenAltura();

        Comparator <Pais> oNombre1 = (p1,p2) -> p1.getNombre().compareTo(p2.getNombre());
        Comparator <Pais> oContinente1 = (p1,p2) -> p1.getContinente().compareTo(p2.getContinente());
        Comparator <Pais> oAltura1 = (p1,p2) -> Double.compare(p1.getAltura(),p2.getAltura());


        // Otra forma (Programacion funcional
        Comparator <Pais> oNombre2 = Comparator.comparing(Pais::getNombre);


        Comparator <Pais> comp = oContinente
                .thenComparing(oAltura.reversed())
                .thenComparing(oNombre.reversed());
        //lista.sort(comp);

        // Orden natural
        //Set <Pais> sp =new TreeSet<>(lista);

        //Orden continente;
        Set<Pais> spa = new TreeSet<>(oContinente);
        spa.addAll(lista);



        for (Pais pais : lista) {
            System.out.println(pais);
        }


        System.out.println("En Europa");
        Predicate <Pais> pred2 = p -> p.getContinente().equals("Europe");
        List <Pais> lista2 = paises.selecciona(pred2);
        Comparator <Pais> comp2 = oAltura;
        Collections.sort(lista2, comp2);
        for (Pais pais : lista2) {
            System.out.println(pais);
        }

        System.out.println("Conjuntos");
        System.out.println(paises.getContinentes());
        System.out.println(paises.getAltura());


        System.out.println("Correspondencias");
        Map<String, List<Pais>> map = paises.porContinentes();
        for (Map.Entry<String, List<Pais>> entry : map.entrySet()) {
            System.out.println(entry.getKey());
            for (Pais pais : entry.getValue()) {
                System.out.println("\t" + pais);
            }
        }

        System.out.println("\n Continentes por altura");
        Map <Double, Set<String>> map2 = paises.continentesPorAltura();
        for (Map.Entry<Double, Set<String>> entry : map2.entrySet()) {
            System.out.print(entry.getKey()+" ");
            for (String continente : entry.getValue()) {
                System.out.print(continente + " ");

            }
            System.out.println();
        }

        System.out.println("\n\n\nTrabajo con Streams\n");
        paises.getPaises()
                .stream()
                .filter(p -> p.getContinente().equals("Europe"))
                .forEach(System.out::println);

        System.out.println(paises.getPaises()
                                        .stream()
                                        .filter(p -> p.getContinente().equals("Europe"))
                                        .filter(p -> p.getNombre().indexOf("a")>=0)
                                        .peek(System.out::println)
                                        .mapToDouble(Pais::getAltura)
                                        .average()
                                        .getAsDouble()
        );

        List <Pais> enEuropa = paises.getPaises().stream()
                                        .filter(p -> p.getContinente().equals("Europe"))
                                        .collect(Collectors.toList());


        Map <String, Set<Double>> ppc = paises.getPaises().stream()
                        .collect(
                                Collectors.groupingBy(
                                        Pais::getContinente,
                                        TreeMap::new,
                                        Collectors.mapping(
                                                Pais::getAltura,
                                                Collectors.toCollection(TreeSet::new)
                                ))
                        );

        System.out.println();
        System.out.println(ppc);


    }
}
