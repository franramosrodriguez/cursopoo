package org.uma.mbd.mdRelojArena.reloj;

public class RelojArena {
    private final int tiempoTotal;
    private int tiempoRestante;

    public RelojArena (int tiempo) {
        tiempoTotal= tiempo;
        tiempoRestante=0;
    }

    public void gira () {
        tiempoRestante = tiempoTotal - tiempoRestante;
    }

    public int getTiempoRestante() {
        return tiempoRestante;
    }

    public void pasaTiempo () {
        tiempoRestante= 0;
    }

    public void pasaTiempo (RelojArena r) {
        int dif = tiempoRestante - r.tiempoRestante;
        if (dif >0) {
            tiempoRestante -= r.tiempoRestante;
            r.tiempoRestante=0;
        } else {
            tiempoRestante = 0;
            r.tiempoRestante=0;
        }
    }
    @Override
    public String toString() {
        return "R("+ tiempoRestante + ", " + (tiempoTotal - tiempoRestante) + ")";
    }


}
