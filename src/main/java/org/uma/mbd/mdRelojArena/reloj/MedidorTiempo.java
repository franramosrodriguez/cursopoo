package org.uma.mbd.mdRelojArena.reloj;

public class MedidorTiempo {
    private RelojArena izdo;
    private RelojArena drcho;
    private int tiempoTotal;

    public MedidorTiempo(int ttA, int ttB) {
        izdo = new RelojArena(ttA);
        drcho = new RelojArena(ttB);
        tiempoTotal =0;
    }

    public void giraDerecho() {
        drcho.gira();
        pasaTiempo();
    }

    public void giraIzquierdo() {
        izdo.gira();
        pasaTiempo();
    }
    public void giraAmbos() {
        izdo.gira();
        drcho.gira();
        pasaTiempo();
    }

    public void pasaTiempo () {

        if (izdo.getTiempoRestante() == 0) {
            tiempoTotal += drcho.getTiempoRestante();
            drcho.pasaTiempo();
        } else if (drcho.getTiempoRestante() == 0) {
            tiempoTotal += izdo.getTiempoRestante();
            izdo.pasaTiempo();
        } else {
            // Ninguno es cero.
            if (izdo.getTiempoRestante() > drcho.getTiempoRestante()) {
                izdo.pasaTiempo(drcho);
                tiempoTotal += drcho.getTiempoRestante();
            } else {
                drcho.pasaTiempo(izdo);
                tiempoTotal += izdo.getTiempoRestante();
            }
        }
    }

    public int getTiempoTotal() {
        return tiempoTotal;
    }

    @Override
    public String toString() {
        return "MT( izdo = " + izdo + ", drcho = " + drcho + ")";
    }

}
